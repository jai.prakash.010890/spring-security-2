package in.jaiprakash.springsecurityauth.controller;

import in.jaiprakash.springsecurityauth.models.AuthenticationRequest;
import in.jaiprakash.springsecurityauth.models.AuthenticationResponse;
import in.jaiprakash.springsecurityauth.util.JWTUtillity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloResource {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JWTUtillity jwtUtillity;

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }


    /**
     * To authenticate we need to have hold of authentication manager
     *
     * To send jwt token we need to use Userdetails service so we will autowire it
     * so we need to autowire it
     * @param authenticationRequest
     * @return
     *
     * As all endpoints need authentication, so we need to tell Spring that this endpoint should be
     */
    @PostMapping(value="/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(), authenticationRequest.getPassword()
            ));
        }catch (BadCredentialsException e){
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtUtillity.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
}
